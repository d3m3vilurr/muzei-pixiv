Pixiv for Muzei
===============

**Disclaimer: It's not a part of official products of Pixiv.**

<img src="preview.png" width="400" height="600">

Muzei art source extension for [Pixiv][].

This extension for Roman Nurik's *[Muzei][] Live Wallpaper* app uses some
unofficial APIs from Pixiv to fetch random artworks.

Written by [Hong Minhee][], and distributed under [GPLv3][] or any later version.

This is not a standalone app.  It requires Muzei app to be installed.
https://play.google.com/store/apps/details?id=net.nurik.roman.muzei

[![Get it on Google Play][badge]][play]

[Pixiv]: http://www.pixiv.com/
[Muzei]: http://www.muzei.co/
[Hong Minhee]: http://dahlia.kr/
[GPLv3]: http://www.gnu.org/licenses/gpl-3.0.html
[play]: https://play.google.com/store/apps/details?id=com.pixiv.muzei.pixivsource
[badge]: https://developer.android.com/images/brand/en_generic_rgb_wo_45.png


Changelog
---------

### Version 1.0.1

To be released.

- Change interval settings.


### Version 1.0.0

Released on March 3, 2014.  Initial release.